package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        byte [] famousNumbers = { 0, 1, 2, 4, 8, 16, 32, 64 } ;
        String arraysAsString = Arrays.toString(famousNumbers);
        System.out.println(arraysAsString);// [0,1,2,4,8,16,32,64]

        long [] bigNumbers = { 2000000L, 3000000L, 1000000000L };
        Arrays.sort(bigNumbers);
        System.out.println(Arrays.toString(bigNumbers));// [200000, 3000000, 10000000].

        int [] number1 = { 1, 2, 5, 8 };
        int [] number2 = { 1, 5, 6};
        int [] number3 = { 1, 2, 5, 8 };
        System.out.println(Arrays.equals(number1,number2));//false
        System.out.println(Arrays.equals(number1, number3));//true

        int size = 10;
        char [] characters = new char[size];
        Arrays.fill(characters, 0, size / 2 , 'A');
        Arrays.fill(characters, size / 2, size, 'B');
        System.out.println(Arrays.toString(characters));//[A,A,A,A,A, B,B,B,B,B ].
        
    }
}
